package com.omegabeamz.newmod.client.gui;

import com.omegabeamz.newmod.client.container.ContainerDeployer;
import com.omegabeamz.newmod.reference.Reference;
import com.omegabeamz.newmod.tileentity.TileEntityDeployer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiDeployer extends GuiContainer
{
    public static final ResourceLocation texture = new ResourceLocation(Reference.MOD_ID.toLowerCase(), Reference.DEPLOYER_TEXTURE);

    public GuiDeployer(InventoryPlayer invPlayer, TileEntityDeployer entity)
    {
        super(new ContainerDeployer(invPlayer, entity));

        xSize = 176;
        ySize = 165;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int j, int i)
    {
        GL11.glColor4f(1F, 1F, 1F, 1F);

        Minecraft.getMinecraft().getTextureManager().bindTexture(texture);

        drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
    }
}
