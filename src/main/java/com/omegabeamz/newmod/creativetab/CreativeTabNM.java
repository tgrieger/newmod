package com.omegabeamz.newmod.creativetab;

import com.omegabeamz.newmod.init.ModItems;
import com.omegabeamz.newmod.reference.Reference;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CreativeTabNM
{
    public static final CreativeTabs NM_TAB = new CreativeTabs(Reference.MOD_ID)
    {
        @Override
        public Item getTabIconItem()
        {
            return ModItems.mapleLeaf;
        }

        @Override
        public String getTranslatedTabLabel()
        {
            return "New Mod";
        }
    };
}
