package com.omegabeamz.newmod.init;

import com.omegabeamz.newmod.NewMod;
import com.omegabeamz.newmod.entity.EntityThrowingRock;
import com.omegabeamz.newmod.handler.ConfigurationHandler;
import com.omegabeamz.newmod.item.*;
import com.omegabeamz.newmod.reference.Names;
import com.omegabeamz.newmod.reference.Reference;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;

@GameRegistry.ObjectHolder(Reference.MOD_ID)
public class ModItems
{
    public static final ItemNM basicBag = new ItemBasicBag();
    public static final ItemNM mapleLeaf = new ItemMapleLeaf();

    // Misc Items
    //
    public static final Item magicBag = new ItemMagicBag();
    public static final Item useMana = new ItemUseMana();
    public static final Item throwingRock = new ItemThrowingRock();
    public static final Item wabbajack = new ItemWabbajack();

    // Armor Items
    //
    public static final Item wizardHat = new ItemWizardArmor(NMArmorMaterials.armorWool, NewMod.proxy.addArmor("wizard"), 0).setUnlocalizedName(Names.Items.WIZARDHAT);
    public static final Item wizardRobe = new ItemWizardArmor(NMArmorMaterials.armorWool, NewMod.proxy.addArmor("wizard"), 1).setUnlocalizedName(Names.Items.WIZARDROBE);
    public static final Item wizardPants = new ItemWizardArmor(NMArmorMaterials.armorWool, NewMod.proxy.addArmor("wizard"), 2).setUnlocalizedName(Names.Items.WIZARDPANTS);
    public static final Item wizardBoots = new ItemWizardArmor(NMArmorMaterials.armorWool, NewMod.proxy.addArmor("wizard"), 3).setUnlocalizedName(Names.Items.WIZARDBOOTS);

    public static void init()
    {
        GameRegistry.registerItem(basicBag, Names.Items.BASICBAG);
        GameRegistry.registerItem(mapleLeaf, Names.Items.MAPLELEAF);
        GameRegistry.registerItem(magicBag, Names.Items.MAGICBAG);
        GameRegistry.registerItem(useMana, Names.Items.USEMANA);
        GameRegistry.registerItem(throwingRock, Names.Items.THROWINGROCK);
        EntityRegistry.registerModEntity(EntityThrowingRock.class, "Throwing Rock", ++NewMod.modEntityIndex, NewMod.instance, 64, 10, true);
        GameRegistry.registerItem(wabbajack, Names.Items.WABBAJACK);

        if(ConfigurationHandler.wizardArmorFlag)
        {
            GameRegistry.registerItem(wizardHat, Names.Items.WIZARDHAT);
            GameRegistry.registerItem(wizardRobe, Names.Items.WIZARDROBE);
            GameRegistry.registerItem(wizardPants, Names.Items.WIZARDPANTS);
            GameRegistry.registerItem(wizardBoots, Names.Items.WIZARDBOOTS);
        }
    }
}
