package com.omegabeamz.newmod.init;

import com.omegabeamz.newmod.handler.ConfigurationHandler;
import com.omegabeamz.newmod.item.crafting.RecipesAll;
import com.omegabeamz.newmod.item.crafting.RecipesWizardArmorDyes;
import com.omegabeamz.newmod.network.PacketDispatcher;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class Recipes
{
    public static void init()
    {
        //GameRegistry.addRecipe(new ItemStack(ModItems.mapleLeaf), " s ", "sss", " s ", 's', new ItemStack(Items.stick));
        //GameRegistry.addShapelessRecipe(new ItemStack(ModBlocks.flag), new ItemStack(ModItems.mapleLeaf), new ItemStack(ModItems.mapleLeaf));
        GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModItems.mapleLeaf), " s ", "sss", " s ", 's', "stickWood"));
        GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(ModBlocks.flag), new ItemStack(ModItems.mapleLeaf), new ItemStack(ModItems.mapleLeaf)));

        GameRegistry.addShapelessRecipe(new ItemStack(ModItems.useMana), Items.diamond);

        if(ConfigurationHandler.wizardArmorFlag)
        {
            CraftingManager.getInstance().getRecipeList().add(new RecipesWizardArmorDyes());
            RecipesAll.instance().addArmorRecipes(CraftingManager.getInstance());
        }
    }
}
