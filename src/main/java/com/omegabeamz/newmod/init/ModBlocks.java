package com.omegabeamz.newmod.init;

import com.omegabeamz.newmod.block.BlockContainerNM;
import com.omegabeamz.newmod.block.BlockDeployer;
import com.omegabeamz.newmod.block.BlockFlag;
import com.omegabeamz.newmod.block.BlockNM;
import com.omegabeamz.newmod.reference.Names;
import com.omegabeamz.newmod.reference.Reference;
import com.omegabeamz.newmod.tileentity.TileEntityDeployer;
import cpw.mods.fml.common.registry.GameRegistry;

@GameRegistry.ObjectHolder(Reference.MOD_ID)
public class ModBlocks
{
    public static final BlockNM flag = new BlockFlag();
    public static final BlockContainerNM deployer = new BlockDeployer();

    public static void init()
    {
        GameRegistry.registerBlock(flag, Names.Blocks.FLAG);
        GameRegistry.registerBlock(deployer, Names.Blocks.DEPLOYER);
        GameRegistry.registerTileEntity(TileEntityDeployer.class, "DeployerNM");
    }
}
