package com.omegabeamz.newmod.init;

import net.minecraft.item.ItemArmor;
import net.minecraftforge.common.util.EnumHelper;

public class NMArmorMaterials
{
    // Armor Materials
    //
    public static final ItemArmor.ArmorMaterial armorWool = EnumHelper.addArmorMaterial("Wool", 5, new int[]{1, 2, 1, 1}, 30);
}
