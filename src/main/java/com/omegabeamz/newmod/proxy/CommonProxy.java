package com.omegabeamz.newmod.proxy;

import java.util.HashMap;
import java.util.Map;

import com.omegabeamz.newmod.NewMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import com.omegabeamz.newmod.client.gui.GuiCustomPlayerInventory;
import com.omegabeamz.newmod.client.gui.GuiMagicBag;
import com.omegabeamz.newmod.entity.ExtendedPlayer;
import com.omegabeamz.newmod.inventory.ContainerCustomPlayer;
import com.omegabeamz.newmod.inventory.ContainerMagicBag;
import com.omegabeamz.newmod.inventory.InventoryMagicBag;
import cpw.mods.fml.common.network.IGuiHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class CommonProxy implements IGuiHandler
{
	/** Used to store IExtendedEntityProperties data temporarily between player death and respawn or dimension change */
	private static final Map<String, NBTTagCompound> extendedEntityData = new HashMap<String, NBTTagCompound>();

	public void registerRenderers() {}

	public int addArmor(String string)
    {
		return 0;
	}
	
	/**
	 * Returns a side-appropriate EntityPlayer for use during message handling
	 */
	public EntityPlayer getPlayerEntity(MessageContext ctx) {
		return ctx.getServerHandler().playerEntity;
	}

	@Override
	public Object getServerGuiElement(int guiId, EntityPlayer player, World world, int x, int y, int z)
    {
		if (guiId == NewMod.GUI_CUSTOM_INV)
        {
			return new ContainerCustomPlayer(player, player.inventory, ExtendedPlayer.get(player).inventory);
		}
        else if (guiId == NewMod.GUI_ITEM_INV)
        {
			return new ContainerMagicBag(player, player.inventory, new InventoryMagicBag(player.getHeldItem()));
		}
        else
        {
			return null;
		}
	}

	@Override
	public Object getClientGuiElement(int guiId, EntityPlayer player, World world, int x, int y, int z)
    {
		if (guiId == NewMod.GUI_CUSTOM_INV)
        {
			return new GuiCustomPlayerInventory(player, player.inventory, ExtendedPlayer.get(player).inventory);
		}
        else if (guiId == NewMod.GUI_ITEM_INV)
        {
			return new GuiMagicBag(player, player.inventory, new InventoryMagicBag(player.getHeldItem()));
		}
        else
        {
			return null;
		}
	}

	/**
	 * Adds an entity's custom data to the map for temporary storage
	 */
	public static void storeEntityData(String name, NBTTagCompound compound)
    {
		extendedEntityData.put(name, compound);
	}

	/**
	 * Removes the compound from the map and returns the NBT tag stored for name or null if none exists
	 */
	public static NBTTagCompound getEntityData(String name)
    {
		return extendedEntityData.remove(name);
	}
}
