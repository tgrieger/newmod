package com.omegabeamz.newmod.reference;

public class Names
{
    public static final class Keys
    {
        public static final String CATEGORY = "keys.newmod.category";
        public static final String CHARGE = "keys.newmod.charge";
        public static final String RELEASE = "keys.newmod.release";
        public static final String INVENTORY = "keys.newmod.inventory";
    }

    public static final class Items
    {
        public static final String BASICBAG = "basicBag";
        public static final String MAPLELEAF = "mapleLeaf";
        public static final String MAGICBAG = "magic_bag";
        public static final String USEMANA = "use_mana";
        public static final String THROWINGROCK = "throwingRock";
        public static final String WABBAJACK = "wabbajack";
        public static final String WIZARDHAT = "wizard_hat";
        public static final String WIZARDROBE = "wizard_robe";
        public static final String WIZARDPANTS = "wizard_pants";
        public static final String WIZARDBOOTS = "wizard_boots";
    }

    public static final class Blocks
    {
        public static final String FLAG = "flag";
        public static final String DEPLOYER = "deployer";
    }
}
