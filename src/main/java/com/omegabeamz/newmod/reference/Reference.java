package com.omegabeamz.newmod.reference;

public class Reference
{
    public static final String MOD_ID = "NewMod";
    public static final String MOD_NAME = "New Mod";
    public static final String VERSION = "1.7.10-1.0";
    public static final String COMMON_PROXY_CLASS = "com.omegabeamz.newmod.proxy.CommonProxy";
    public static final String CLIENT_PROXY_ClASS = "com.omegabeamz.newmod.proxy.ClientProxy";
    public static final String GUI_FACTORY_CLASS = "com.omegabeamz.newmod.client.gui.GuiFactory";
    public static final String DEPLOYER_TEXTURE = "textures/gui/deployer.png";
    public static final String[] INVENTORY_TEXTURE =
            {
                    "textures/gui/inventory.png",
                    "textures/gui/inventory1.png",
                    "textures/gui/inventory2.png",
                    "textures/gui/inventory3.png",
                    "textures/gui/inventory4.png"
            };
    public static int equippedBags = 0;
    public static final String MAGIC_BAG_INVENTORY_TEXTURE = "textures/gui/magic_bag.png";
    public static final String MANA_BAR_TEXTURE = "textures/gui/mana_bar.png";
    public static final String WIZARD_LAYER_1_TEXTURE = MOD_ID.toLowerCase() + ":textures/models/armor/wizard_layer_1.png";
    public static final String WIZARD_LAYER_1_OVERLAY_TEXTURE = MOD_ID.toLowerCase() + ":textures/models/armor/wizard_layer_1_overlary.png";
    public static final String WIZARD_LAYER_2_TEXTURE = MOD_ID.toLowerCase() + ":textures/models/armor/wizard_layer_2.png";
    public static final String WIZARD_LAYER_2_OVERLAY_TEXTURE = MOD_ID.toLowerCase() + ":textures/models/armor/wizard_layer_2_overlary.png";
    public static final String EXT_PROP_NAME = "ExtendedPlayer";
}
