package com.omegabeamz.newmod.reference;

public enum Key
{
    UNKNOWN, CHARGE, RELEASE;
}
