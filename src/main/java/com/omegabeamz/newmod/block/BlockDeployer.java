package com.omegabeamz.newmod.block;

import com.omegabeamz.newmod.NewMod;
import com.omegabeamz.newmod.reference.GUIs;
import com.omegabeamz.newmod.reference.Names;
import com.omegabeamz.newmod.tileentity.TileEntityDeployer;
import cpw.mods.fml.common.network.internal.FMLNetworkHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockDeployer extends BlockContainerNM
{
    public BlockDeployer()
    {
        super();
        this.setBlockName(Names.Blocks.DEPLOYER);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int p_149915_2_) {
        return new TileEntityDeployer();
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
        if(!world.isRemote)
        {
            FMLNetworkHandler.openGui(player, NewMod.instance, GUIs.DEPLOYER_GUI, world, x, y, z);
        }

        return true;
    }
}
