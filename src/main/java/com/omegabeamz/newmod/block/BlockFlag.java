package com.omegabeamz.newmod.block;

import com.omegabeamz.newmod.reference.Names;

public class BlockFlag extends BlockNM
{
    public BlockFlag()
    {
        super();
        this.setBlockName(Names.Blocks.FLAG);
    }
}
