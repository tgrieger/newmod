package com.omegabeamz.newmod.block;

import com.omegabeamz.newmod.creativetab.CreativeTabNM;
import com.omegabeamz.newmod.reference.Reference;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockContainerNM extends BlockContainer
{
    public BlockContainerNM(Material material)
    {
        super(material);
        this.setCreativeTab(CreativeTabNM.NM_TAB);
    }

    public BlockContainerNM()
    {
        this(Material.rock);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int p_149915_2_) {
        return null;
    }

    @Override
    public String getUnlocalizedName()
    {
        return String.format("tile.%s%s", Reference.MOD_ID.toLowerCase() + ":", getUnwrappedlocalizedName(super.getUnlocalizedName()));
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister)
    {
        blockIcon = iconRegister.registerIcon(String.format("%s", getUnwrappedlocalizedName(this.getUnlocalizedName())));
    }

    protected String getUnwrappedlocalizedName(String unlocalizedName)
    {
        return unlocalizedName.substring(unlocalizedName.indexOf(".") + 1);
    }
}
