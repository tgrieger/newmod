package com.omegabeamz.newmod;

import com.omegabeamz.newmod.inventory.InventoryCustomPlayer;
import com.omegabeamz.newmod.reference.Reference;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;

public class ExtendedPlayer implements IExtendedEntityProperties
{
    private final EntityPlayer player;
    public final InventoryCustomPlayer inventory = new InventoryCustomPlayer();

    public ExtendedPlayer(EntityPlayer player)
    {
        this.player = player;
    }

    public static final void register(EntityPlayer player)
    {
        player.registerExtendedProperties(Reference.EXT_PROP_NAME, new ExtendedPlayer(player));
    }

    public static final ExtendedPlayer get(EntityPlayer player)
    {
        return (ExtendedPlayer) player.getExtendedProperties(Reference.EXT_PROP_NAME);
    }

    // Save any custom data that needs saving here
    // ALEX: Possibly how you would implement skill points?
    @Override
    public void saveNBTData(NBTTagCompound compound)
    {
        // We need to create a new tag compound that will save everything for our Extended Properties
        NBTTagCompound properties = new NBTTagCompound();
        this.inventory.writeToNBT(properties);

        // Commenting out the below stuff since I'm not using it, leaving it as an example
        // We only have 2 variables currently; save them both to the new tag
        // properties.setInteger("CurrentMana", this.currentMana);
        // properties.setInteger("MaxMana", this.maxMana);

        // Now add our custom tag to the player's tag with a unique name (our property's name)
        // This will allow you to save multiple types of properties and distinguish between them
        // If you only have one type, it isn't as important, but it will still avoid conflicts between
        // your tag names and vanilla tag names. For instance, if you add some "Items" tag,
        // that will conflict with vanilla. Not good. So just use a unique tag name.
        compound.setTag(Reference.EXT_PROP_NAME, properties);
    }

    @Override
    public void loadNBTData(NBTTagCompound compound)
    {
        // Here we fetch the unique tag compound we set for this class of Extended Properties
        NBTTagCompound properties = (NBTTagCompound) compound.getTag(Reference.EXT_PROP_NAME);
        this.inventory.readFromNBT(properties);
        /*// Get our data from the custom tag compound
        this.currentMana = properties.getInteger("CurrentMana");
        this.maxMana = properties.getInteger("MaxMana");
        // Just so you know it's working, add this line:
        System.out.println("[TUT PROPS] Mana from NBT: " + this.currentMana + "/" + this.maxMana);*/
    }

    @Override
    public void init(Entity entity, World world)
    {
        // NOOP
    }
}
