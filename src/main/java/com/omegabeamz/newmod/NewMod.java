package com.omegabeamz.newmod;

import com.omegabeamz.newmod.handler.ConfigurationHandler;
import com.omegabeamz.newmod.handler.NMEventHandler;
import com.omegabeamz.newmod.init.ModBlocks;
import com.omegabeamz.newmod.init.ModItems;
import com.omegabeamz.newmod.init.Recipes;
import com.omegabeamz.newmod.network.PacketDispatcher;
import com.omegabeamz.newmod.proxy.CommonProxy;
import com.omegabeamz.newmod.reference.Reference;
import com.omegabeamz.newmod.utility.LogHelper;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import net.minecraftforge.common.MinecraftForge;

@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME, version = Reference.VERSION, guiFactory = Reference.GUI_FACTORY_CLASS)
public class NewMod
{
    @Mod.Instance(Reference.MOD_ID)
    public static NewMod instance;

    @SidedProxy(clientSide = Reference.CLIENT_PROXY_ClASS, serverSide = Reference.COMMON_PROXY_CLASS)
    public static CommonProxy proxy;

    // Used to keep track kof GUIs that we make
    //
    private static int modGuiIndex = 10;
    public static final int GUI_CUSTOM_INV = modGuiIndex++;
    public static final int GUI_ITEM_INV = modGuiIndex++;

    // Starting index for all of our mod's item IDs
    //
    public static int modEntityIndex = 0;

    // The method for registering blocks and items
    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        ConfigurationHandler.init(event.getSuggestedConfigurationFile());
        FMLCommonHandler.instance().bus().register(new ConfigurationHandler());

        // Initialize all mod items
        //
        ModItems.init();

        // Initialize all mod blocks
        //
        ModBlocks.init();

        // Remember to register your packets! This applies whether or not yo used a custom class or direct implementation of SimpleNetworkWrapper
        //
        PacketDispatcher.registerPackets();

        LogHelper.info("Pre Initialization Complete!");
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {
        proxy.registerRenderers();
        MinecraftForge.EVENT_BUS.register(new NMEventHandler());
        NetworkRegistry.INSTANCE.registerGuiHandler(this, new CommonProxy());

        // Initialize all the mod recipes
        //
        Recipes.init();

        LogHelper.info("Initialization Complete!");
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
        // Generally, where you interact with other mods

        LogHelper.info("Post Initialization Complete!");
    }
}
