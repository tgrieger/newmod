package com.omegabeamz.newmod.item;

import com.omegabeamz.newmod.creativetab.CreativeTabNM;
import com.omegabeamz.newmod.reference.Names;

public class ItemMapleLeaf extends ItemNM
{
    public ItemMapleLeaf()
    {
        super();
        this.setUnlocalizedName(Names.Items.MAPLELEAF);
        this.setCreativeTab(CreativeTabNM.NM_TAB);
    }
}
