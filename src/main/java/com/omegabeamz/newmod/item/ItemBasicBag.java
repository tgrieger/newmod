package com.omegabeamz.newmod.item;

import com.omegabeamz.newmod.creativetab.CreativeTabNM;
import com.omegabeamz.newmod.reference.Names;

public class ItemBasicBag extends ItemNM
{
    public ItemBasicBag()
    {
        super();
        this.setUnlocalizedName(Names.Items.BASICBAG);
        this.maxStackSize = 1;
    }
}
