package com.omegabeamz.newmod.item;

import com.omegabeamz.newmod.reference.Names;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import com.omegabeamz.newmod.entity.ExtendedPlayer;

public class ItemUseMana extends ItemNM
{
	public ItemUseMana() {
		super();
		setMaxDamage(0);
		setMaxStackSize(1);
        setUnlocalizedName(Names.Items.USEMANA);
	}

	@Override
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
		if (!world.isRemote) {
			ExtendedPlayer props = ExtendedPlayer.get(player);
			if (props.consumeMana(15)) {
				System.out.println("[MANA ITEM] Player had enough mana. Do something awesome!");
			} else {
				System.out.println("[MANA ITEM] Player ran out of mana. Sad face.");
				props.replenishMana();
			}
		}

		return stack;
	}
}