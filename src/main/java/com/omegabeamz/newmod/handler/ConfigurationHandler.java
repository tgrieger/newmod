package com.omegabeamz.newmod.handler;

import com.omegabeamz.newmod.reference.Reference;
import cpw.mods.fml.client.event.ConfigChangedEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.common.config.Configuration;

import java.io.File;

public class ConfigurationHandler
{
    public static Configuration configuration;
    public static boolean wizardArmorFlag = true;

    public static void init(File configFile)
    {
        // Create the configuration object from the given configFile
        if(configuration ==  null)
        {
            configuration = new Configuration(configFile);
            loadConfiguration();
        }
    }

    @SubscribeEvent
    public void onConfigurationChangedEvent(ConfigChangedEvent.OnConfigChangedEvent event)
    {
        if(event.modID.equalsIgnoreCase(Reference.MOD_ID))
        {
            loadConfiguration();
        }
    }

    private static void loadConfiguration()
    {
        wizardArmorFlag = configuration.getBoolean("Wizard Armor", Configuration.CATEGORY_GENERAL, true, "Toggles wizard armor on or off");

        if(configuration.hasChanged())
        {
            configuration.save();
        }
    }
}
