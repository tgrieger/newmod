package com.omegabeamz.newmod.inventory;

import com.omegabeamz.newmod.NewMod;
import com.omegabeamz.newmod.client.gui.GuiCustomPlayerInventory;
import com.omegabeamz.newmod.item.ItemBasicBag;
import com.omegabeamz.newmod.reference.Reference;
import com.omegabeamz.newmod.utility.LogHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotBasicBag extends Slot
{
    private EntityPlayer player;
    private InventoryCustomPlayer inventory;
    private int bagCount = 0;

	public SlotBasicBag(InventoryCustomPlayer inventory, int slotIndex, int x, int y, EntityPlayer player)
    {
		super(inventory, slotIndex, x, y);
        this.player = player;
        this.inventory = inventory;
	}
	
	/**
	 * Check if the stack is a valid item for this slot. Always true beside for the armor slots
	 * (and now also not always true for our custom inventory slots)
	 */
	@Override
	public boolean isItemValid(ItemStack stack)
    {
		// We only want our custom item to be storable in this slot
        if(stack == null)
        {
            return false;
        }
		return stack.getItem() instanceof ItemBasicBag;
	}

    @Override
    public void onSlotChanged()
    {
        super.onSlotChanged();
        bagCount = Reference.equippedBags;
        Reference.equippedBags = 0;
        for(int i = 0; i < inventory.getSizeInventory(); i++)
        {
            if(inventory.getStackInSlot(i) != null && inventory.getStackInSlot(i).getItem() instanceof ItemBasicBag)
            {
                Reference.equippedBags++;
            }
        }
        if(Reference.equippedBags != bagCount)
        {
            //NewMod.proxy.getServerGuiElement(NewMod.GUI_CUSTOM_INV, player, player.worldObj, (int) player.posX, (int) player.posY, (int) player.posZ);
            //NewMod.proxy.getClientGuiElement(NewMod.GUI_CUSTOM_INV, player, player.worldObj, (int) player.posX, (int) player.posY, (int) player.posZ);
            //player.closeScreen();
            //Minecraft.getMinecraft().displayGuiScreen(new GuiCustomPlayerInventory(player, player.inventory, inventory));
            //player.inventory.setItemStack(inventory.getStackInSlot(this.getSlotIndex()));
            ItemStack itemStack = player.inventory.getItemStack();
            player.openGui(NewMod.instance, NewMod.GUI_CUSTOM_INV, player.worldObj, (int) player.posX, (int) player.posY, (int) player.posZ);
            player.inventory.setItemStack(itemStack);
        }
    }

    /*@Override
    public void markDirty()
    {
        for (int i = 0; i < inventory.getSizeInventory(); ++i)
        {
            if (inventory.getStackInSlot(i) != null && inventory.getStackInSlot(i).stackSize == 0)
            {
                inventory.setInventorySlotContents(i, null);
            }
        }
        inventory.writeToNBT(invStack.getTagCompound());
    }*/

    @Override
    public int getSlotStackLimit()
    {
        return 1;
    }
}
