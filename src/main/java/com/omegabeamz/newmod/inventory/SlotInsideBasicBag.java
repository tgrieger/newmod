package com.omegabeamz.newmod.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;

public class SlotInsideBasicBag extends Slot
{
    public SlotInsideBasicBag(InventoryCustomPlayer inventory, int slotIndex, int x, int y)
    {
        super(inventory, slotIndex, x, y);
    }
}
