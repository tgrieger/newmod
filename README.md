# README #

What is needed:

- IntelliJ version 13 (latest is fine), NOT 14.
- For the JAVA jdk, I'm using 1.7, not sure if 1.8 will work.
- Minecraft 1.7.10

Commands to run (from repository root):

gradlew setupDecompWorkspace

gradlew idea
	
Note: Use "eclipse" instead of "idea" if using eclipse.
	
After the commands are done running, use your IDE of choice to OPEN, not IMPORT, and select the build.gradle file.

In IntelliJ, to add git to your right click menus, got to File -> Settings -> Version Control and look for a red banner that has a link that says "add root", or something to that effect.